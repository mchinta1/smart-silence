# README #


* Quick summary:
 Smart silence is a app to set the phone in silent mode based on ambient sound surrounded by the device 
 and the users the activity (moving/not moving). It detects a location at which the phone was put in silent 
 mode and after a few repeated actions the phone is automatically set in silent mode. 
 Uses the microphone, GPS and Accelerometer. 

* Version : Smart Silence 0.1

***** IMP NOTE ***** -- Please use android Marshmallow v6.0 or Nougat v7.0 to run the app (Tested only on these platforms)******

* Status : Reading of values 
-> GPS Sensor     : Done. 
-> Accelerometer  : Done. (200 readings with sampling rate of 12 seconds)
-> Microphone     : Done. (Surrounding noise should be less than the 80dB)

### How do I get set up? ###

Build the project with Gradle, 
click run -> If app crashes, (turn on location)
check permissions required for the app in the settings give all the required permissions
additionally required Do not disturb access, go to settings and accept the access.
and finally requires data connection or wifi, please provide them. 

before start of the application, make sure the phone is not in silent or vibration.
once the app starts put the phone in silent mode, it detects the gps location and makes entry as the silent zone.

Now quit the app, put the mobile back to normal mode
now launch the app it identifies the current location as the silent zone and tracks the user's action by accelerometer (moving/non moving)
if not moving checks for near ambient sound surrounded by if <80dB then mobile turns to silent mode. 

Please find the demo of the app in the following link below
https://drive.google.com/a/binghamton.edu/file/d/0B4TGgS14AmpVcFhDdlAtSExBb0lSOXlJTVQ1ajgyNktTM0c4/view?usp=sharing


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

![Activity Recognition-1.png](https://bitbucket.org/repo/ngEed45/images/205743814-Activity%20Recognition-1.png)![Activity Recognition 2.png](https://bitbucket.org/repo/ngEed45/images/424449580-Activity%20Recognition%202.png)![Activity Recognition 3.png](https://bitbucket.org/repo/ngEed45/images/1498935300-Activity%20Recognition%203.png)![Microphone Processing -1.png](https://bitbucket.org/repo/ngEed45/images/1008373446-Microphone%20Processing%20-1.png)![Microphone Data processing 2.png](https://bitbucket.org/repo/ngEed45/images/2299795420-Microphone%20Data%20processing%202.png)